﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FirstCsharp
{
    class Data
    {
        //private int oldBlz;
        //private string oldPlace;
        //private int result;

        public String NumberLines(String path, StreamReader file) //return number of lines
        {
            int counter = 0;
            string line = null;

            while ((line = file.ReadLine()) != null)
            {
                //count the number of lines
                counter++;
            }

            return counter.ToString();
        }

        public DataTable ConvertToDataTable(string filePath) // convert txtFile to datatable
        {
            DataTable tbl = new DataTable("Data");
            //insert head
            tbl.Columns.Add("BLZ", typeof(Int32));
            tbl.Columns.Add("Place", typeof(string));
            tbl.Columns.Add("Number", typeof(int));

            //read the file
            string[] lines = System.IO.File.ReadAllLines(filePath);


            foreach (string line in lines)
            {
                //only blz
                int blz = Convert.ToInt32(line.Substring(0, 8));
                // only place
                string place = line.Substring(72, 35).Trim(' ');
                tbl.Rows.Add(blz, place);
            }

            return tbl;
        }
        public void WriteCSVandConvert(DataTable dt) // convert datatable to CSV and count the same BLZ
        {
            StringBuilder sb = new StringBuilder();
            //TODO https://stackoverflow.com/questions/24387610/getting-count-of-all-repeated-rows-in-a-datatable-in-c-sharp

            DataTable tblN = new DataTable("Data");
            tblN.Columns.Add("BLZ", typeof(Int32));
            tblN.Columns.Add("Place", typeof(string));
            tblN.Columns.Add("Number", typeof(int));

            var query = dt.AsEnumerable().GroupBy(r => new { BLZ = r.Field<Int32>("BLZ"), Place = r.Field<string>("Place") }).Select(grp => new { BLZ = grp.Key.BLZ, Place = grp.Key.Place, Count = grp.Count() });
            foreach (var item in query)
            {
                tblN.Rows.Add(item.BLZ, item.Place, item.Count);
            }

            IEnumerable<string> columnNames = tblN.Columns.Cast<DataColumn>().Select(column => column.ColumnName);
            sb.AppendLine(string.Join(",", columnNames));


            foreach (DataRow row in tblN.Rows)
            {
                IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                // What is  "row.ItemArray.Select(field => field.ToString())" ?
                sb.AppendLine(string.Join(",", fields));
            }

            File.WriteAllText("output.csv", sb.ToString());
        }
    }
}
