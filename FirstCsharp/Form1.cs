﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.IO.Compression;

namespace FirstCsharp
{
    public partial class Form1 : Form
    {
        String path = @"./";
        //String filename;
        //String filenameN;
        //StreamReader file = new StreamReader(@".\BLZ.txt",);


        public Form1()
        {
            InitializeComponent();
        }

        private void btnstart_Click(object sender, EventArgs e)
        {
            String filename = Path.GetFileName(tBurl.Text);
            WebClient webClient = new WebClient();
            
            // texfield must not be empty
            if (!string.IsNullOrEmpty(tBurl.Text))
            {
                Uri url = new Uri(tBurl.Text); //https://www.bundesbank.de/resource/blob/602678/a14941baad2fa3a56a3390d601419d49/mL/blz-aktuell-txt-zip-data.zip
                // if the file alredy exist 
                if (!File.Exists(filename))
                {
                    webClient.DownloadFileAsync(url, filename);
                    webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);
                }
            }

        }
        private void Completed(object sender, AsyncCompletedEventArgs e)
        {
            MessageBox.Show("Download completed");
            // name of Zipfile
            String fileNameZip = Path.GetFileName(tBurl.Text);
            ZipFile.ExtractToDirectory(path + fileNameZip, path);
        }

        private void btncheck_Click(object sender, EventArgs e)
        {
            Data info = new Data();
            StreamReader tmp = new StreamReader(@".\BLZ.txt");
            tBdatasets.Text = info.NumberLines(path, tmp);
            //info.ConvertToDataTable("BLZ.txt"); // Convert.ToInt32(info.NumberLines(path, file)));
            //TODO: Sort BLZ by Place
            info.WriteCSVandConvert(info.ConvertToDataTable("BLZ.txt"));
        }
    }






}
